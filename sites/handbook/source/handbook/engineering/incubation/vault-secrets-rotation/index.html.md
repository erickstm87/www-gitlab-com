---
layout: handbook-page-toc
title: Vault Secrets Rotaion Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##  Vault Secrets Rotaion Single-Engineer Group

The  Vault Secret Rotaion SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).  Our aim is to create a secrets management solution that automatically rotates secrets in both CI pipelines at runtime, as well as into an already deployed running application. We can iterate towards this solution by integrating with existing well known tools.


